#!/bin/bash
#v.1.0

set -e
tags=( $@ )
len=${#tags[@]}

env=dev
image=dev-test-java

#Comment or uncomment this depending on env
#Put your own profiles values for these
#dev
#aws_profile=juanpa.dev
#aws_region=us-west-2
#aws_account=214997541444

#Solara dev
aws_profile=juanpa.solara.dev
aws_region=us-west-2
aws_account=339712748353

#stage
#aws_profile=juan.rivera.swap.stage
#aws_region=us-west-2
#aws_account=220009258125

aws_url=$aws_account.dkr.ecr.$aws_region.amazonaws.com

printHelp() {
  echo
  echo "HELP for 'push-docker-image.sh'"
  echo
  echo "Usage: ./push-docker-image.sh [<tag>] [,<tag>]"
  echo
  echo "Push a docker image to AWS ECR repository"
  echo
  echo "Script takes takes zero, one, or more tags."
  echo "It pushes the docker image from your local repository into our AWS ECS repository."
  echo "It tags the docker image with the tag(s) that you provide."
  echo "If no tags are provided, 'latest' is assumed to be the single tag to be used."
  echo
  echo "NOTE: Your local image must already be tagged with the *first* tag provided."
  echo "You can use the 'tag-docker-image.sh' script to add a needed tag to your local image."
  echo
  echo "Note: DockerImageName file is required to be in the same directory and contains the name of the docker image."
  echo
}

processOtherTags() {
  for otherTag in "$@" ; do
    echo "Tagging $image:$firstTag for remote push as $aws_url/$image:$firstTag..."
    docker tag $aws_url/$image:$firstTag $aws_url/$image:$otherTag

    echo "Pushing image remotely..."
    docker push $aws_url/$image:$otherTag

    echo "Removing remote url tagging of $aws_url/$image:$otherTag..."
    docker rmi $aws_url/$image:$otherTag
	done
}

if [ "$1" == "-?" ] || [ "$1" == "-h" ] ||  [ "$1" == "--h" ] || [ "$1" == "help" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]
then
  printHelp
  exit 0
fi

if [ $len == 0 ]
then
  echo
  echo "No tags provided, assuming at single tag of 'latest'."
  firstTag=latest
else
  firstTag=${tags[0]}
fi

aws ecr --profile $aws_profile get-login-password --region $aws_region | docker login --username AWS --password-stdin $aws_url/

imageid="$(docker images -q $image)"

echo "Tagging $image:$firstTag for remote push as $aws_url/$image:$firstTag..."
docker tag $image:$firstTag $aws_url/$image:$firstTag

echo "Pushing image remotely..."
docker push $aws_url/$image:$firstTag

if [ $len -gt 1 ]
then
  otherTags=("${tags[@]:1:$len}")
  processOtherTags "${otherTags[@]}"
fi

echo "Removing remote url tagging of $aws_url/$image:$firstTag..."
docker rmi $aws_url/$image:$firstTag
