package com.finance;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class ApplicationTest{

    @Test
    public void testApplication() {
        Application app = new Application();
        assertNotNull(app);
        app.cleanup();
        assertNotNull(app);
    }
}
