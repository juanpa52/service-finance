package com.finance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.finance.Application;
import com.finance.dao.PaymentRequestDAO;
import com.finance.dao.PaymentResponseDAO;
import com.finance.pojo.ErrorField;
import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PaymentController.class, Application.class})
@WebAppConfiguration
@ActiveProfiles("integration")
public class PaymentControllerTest {
    public static final String ID_1 = "11326e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final String ID_2 = "f014ef1d-53bd-43ee-99a1-4d1c3977be8a";

    private static MockMvc mockMvc;
    private static List<String> expectedPaymentIds;

    @Autowired
    private PaymentController paymentController;

    @MockBean
    private PaymentRequestDAO paymentRequestDAO;

    @MockBean
    private PaymentResponseDAO paymentResponseDAO;

    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper mapper;

    @Autowired
    private MappingJackson2HttpMessageConverter jackson2HttpMessageConverter;

    @Autowired
    private ControllerExceptionHandler controllerExceptionHandler;

    @BeforeClass
    public static void setup() {
    }

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(paymentController)
                .setMessageConverters(jackson2HttpMessageConverter)
                .setControllerAdvice(controllerExceptionHandler)
                .build();

        expectedPaymentIds = List.of(ID_1, ID_2);
    }

    @Test
    public void postCreatePaymentsShouldReturnCreated() throws Exception {
        //Given
        Double amount = 100.00;
        int terms = 2;
        Double rate = 5.00;

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .amount(amount)
                .rate(rate)
                .terms(terms)
                .build();

        when(paymentRequestDAO.savePayment(any())).thenReturn(1);
        when(paymentResponseDAO.savePaymentResponses(anyList())).thenReturn(expectedPaymentIds);

        //When
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/payments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(paymentRequest)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        //Then
        CollectionType listType = mapper.getTypeFactory().constructCollectionType(List.class, PaymentResponse.class);
        List<PaymentResponse> returnedResponses = mapper.readValue(mvcResult.getResponse().getContentAsString(), listType);
        assertEquals(2, returnedResponses.size());
    }

    @Test
    public void postCreatePaymentsWithInvalidParamsShouldReturnBadRequest() throws Exception {
        //Given
        Double amount = 0.0;
        int terms = 200;
        Double rate = 0.0001;
        String expectedError1 = "must be between 1 and 100";
        String expectedError2 = "must be greater than or equal to 0.1";
        String expectedError3 = "must be greater than or equal to 0.00001";

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .amount(amount)
                .rate(rate)
                .terms(terms)
                .build();

        //When
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/payments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(paymentRequest)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString(expectedError1)))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString(expectedError2)))
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString(expectedError3)))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        //Then
        CollectionType listType = mapper.getTypeFactory().constructCollectionType(List.class, ErrorField.class);
        List<ErrorField> returnedResponses = mapper.readValue(mvcResult.getResponse().getContentAsString(), listType);
        assertEquals(3, returnedResponses.size());
    }

    @Test
    public void postCreatePaymentsWithInternalSaveErrorShouldReturnServUnavailable() throws Exception {
        //Given
        Double amount = 100.00;
        int terms = 2;
        Double rate = 5.00;
        String expectedError = "Service unavailable";

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .amount(amount)
                .rate(rate)
                .terms(terms)
                .build();

        when(paymentRequestDAO.savePayment(any())).thenReturn(1);
        when(paymentResponseDAO.savePaymentResponses(anyList())).thenReturn(Collections.emptyList());

        //When //Then
        mockMvc.perform(MockMvcRequestBuilders.post("/payments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(paymentRequest)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isServiceUnavailable())
                .andExpect(MockMvcResultMatchers.content().string(StringContains.containsString(expectedError)))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
    }
}
