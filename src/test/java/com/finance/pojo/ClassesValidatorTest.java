package com.finance.pojo;


import com.finance.exception.UnexpectedException;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Assertions;
import pl.pojo.tester.api.assertion.Method;

public class ClassesValidatorTest {

    private static final Class<?>[] CLASSES_TO_TEST = {
            PaymentRequest.class,
            PaymentResponse.class,
            BaseEntity.class,
            ErrorField.class
    };

    @Test
    public void testPojo() {
        Assertions.assertPojoMethodsForAll(CLASSES_TO_TEST)
                .testing(Method.SETTER, Method.GETTER, Method.EQUALS, Method.TO_STRING, Method.HASH_CODE) //Test all methods
                .areWellImplemented();
    }

    @Test
    public void testException() {
        Assertions.assertPojoMethodsForAll(UnexpectedException.class)
                .testing(Method.SETTER, Method.GETTER, Method.EQUALS, Method.TO_STRING, Method.CONSTRUCTOR) //Test all methods
                .areWellImplemented();
    }

}
