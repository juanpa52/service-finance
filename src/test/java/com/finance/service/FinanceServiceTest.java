package com.finance.service;

import com.finance.Application;
import com.finance.dao.PaymentRequestDAO;
import com.finance.dao.PaymentResponseDAO;
import com.finance.exception.UnexpectedException;
import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("integration")
@Sql(scripts = {"/sql-data/schema.sql", "/sql-data/data.sql"})
public class FinanceServiceTest {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
    public static final String ID_1 = "11326e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final String ID_2 = "f014ef1d-53bd-43ee-99a1-4d1c3977be8a";
    public static final String PAYMENT_ID = "00026e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final int PAYMENT_NUMBER_1 = 1;
    public static final int PAYMENT_NUMBER_2 = 2;
    public static final Double AMOUNT = 100.00;
    public static final int TERMS = 2;
    public static final Double RATE = 5.00;

    private static final Instant CREATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    private static final Instant UPDATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    public static final Instant PAYMENT_DATE_1 = ZonedDateTime.parse("2021-11-08 21:09:31", FORMATTER).toInstant();
    public static final Instant PAYMENT_DATE_2 = ZonedDateTime.parse("2021-11-15 21:09:31", FORMATTER).toInstant();

    @Autowired
    private FinanceService financeService;

    @SpyBean
    private PaymentRequestDAO paymentRequestDAO;

    @SpyBean
    private PaymentResponseDAO paymentResponseDAO;

    @BeforeClass
    public static void initData() {
    }

    @Test
    public void testCreatePaymentSetSuccess() {
        //Given
        String id = UUID.randomUUID().toString();
        Double amount = 100.00;
        int terms = 5;
        Double rate = 5.00;
        ArgumentCaptor<List<PaymentResponse>> valueCapture = ArgumentCaptor.forClass(List.class);

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .id(id)
                .amount(amount)
                .rate(rate)
                .terms(terms)
                .build();

        //When
        List<PaymentResponse> payments = financeService.createPaymentSet(paymentRequest);

        //Then
        assertEquals(terms, payments.size());
        assertEquals(Instant.now().plus(7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS), payments.get(0).getPaymentDate());
        assertEquals(Instant.now().plus(terms * 7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS), payments.get(terms - 1).getPaymentDate());

        verify(paymentRequestDAO, times(1)).savePayment(eq(paymentRequest));
        verify(paymentResponseDAO, times(1)).savePaymentResponses(valueCapture.capture());
        assertEquals(terms, valueCapture.getValue().size());
    }

    @Test(expected = UnexpectedException.class)
    public void testCreatePaymentSetWithUnknownErrorTryingToSaveResponses() {
        //Given
        String id = UUID.randomUUID().toString();
        Double amount = 100.00;
        int terms = 5;
        Double rate = 5.00;
        ArgumentCaptor<List<PaymentResponse>> valueCapture = ArgumentCaptor.forClass(List.class);
        doReturn(Collections.emptyList()).when(paymentResponseDAO).savePaymentResponses(any());

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .id(id)
                .amount(amount)
                .rate(rate)
                .terms(terms)
                .build();

        //When
        try {
            financeService.createPaymentSet(paymentRequest);
        }
        //Then
        catch (Exception e) {
            verify(paymentRequestDAO, times(1)).savePayment(eq(paymentRequest));
            verify(paymentResponseDAO, times(1)).savePaymentResponses(valueCapture.capture());
            throw e;
        }
    }

}
