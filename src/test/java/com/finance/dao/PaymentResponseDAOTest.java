package com.finance.dao;

import com.finance.Application;
import com.finance.pojo.PaymentResponse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("integration")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Sql(scripts = {"/sql-data/schema.sql", "/sql-data/data.sql"})
public class PaymentResponseDAOTest {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
    public static final String ID_1 = "11326e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final String ID_2 = "f014ef1d-53bd-43ee-99a1-4d1c3977be8a";
    public static final String PAYMENT_ID = "00026e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final Double AMOUNT = 55.00;
    public static final int PAYMENT_NUMBER_1 = 1;
    public static final int PAYMENT_NUMBER_2 = 2;

    private static final Instant CREATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    private static final Instant UPDATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    public static final Instant PAYMENT_DATE_1 = ZonedDateTime.parse("2021-11-08 21:09:31", FORMATTER).toInstant();
    public static final Instant PAYMENT_DATE_2 = ZonedDateTime.parse("2021-11-15 21:09:31", FORMATTER).toInstant();

    private static PaymentResponse paymentResponse1;
    private static PaymentResponse paymentResponse2;

    @Autowired
    private PaymentResponseDAO paymentRequestDAO;

    @BeforeClass
    public static void initData() {
        paymentResponse1 = PaymentResponse.builder()
                .id(ID_1)
                .amount(AMOUNT)
                .paymentNumber(PAYMENT_NUMBER_1)
                .paymentDate(PAYMENT_DATE_1)
                .paymentId(PAYMENT_ID)
                .createdAt(CREATED_AT)
                .updatedAt(UPDATED_AT)
                .build();

        paymentResponse2 = PaymentResponse.builder()
                .id(ID_2)
                .amount(AMOUNT)
                .paymentNumber(PAYMENT_NUMBER_2)
                .paymentDate(PAYMENT_DATE_2)
                .paymentId(PAYMENT_ID)
                .createdAt(CREATED_AT)
                .updatedAt(UPDATED_AT)
                .build();
    }

    @Before
    public void init() {
    }

    @Test
    public void testGetPaymentResponseById() {
        //Given
        //When
        PaymentResponse paymentReqReturned = paymentRequestDAO.getPaymentResponseById(ID_1);

        //Then
        assertEquals(paymentResponse1, paymentReqReturned);
    }

    @Test
    public void testGetPaymentResponsesByPaymentId() {
        //Given
        //When
        List<PaymentResponse> paymentReqReturned = paymentRequestDAO.getPaymentResponsesByPaymentId(PAYMENT_ID);

        //Then
        assertEquals(List.of(paymentResponse1, paymentResponse2), paymentReqReturned);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testGetPaymentResponseByIdNullMid() {
        //When
        paymentRequestDAO.getPaymentResponseById(null);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testGetPaymentResponseByNoExistentId() {
        //When
        paymentRequestDAO.getPaymentResponseById("other");
    }

    @Test
    public void testSavePaymentResponse() {
        //Given
        Double newAmount = 200.0;
        String id1 = UUID.randomUUID().toString();
        String id2 = UUID.randomUUID().toString();
        PaymentResponse thirdPaymentResponse = paymentResponse1.toBuilder().id(id1).amount(newAmount).build();
        PaymentResponse fourthPaymentResponse = paymentResponse1.toBuilder().id(id2).amount(newAmount).build();

        //When
        List<String> results = paymentRequestDAO.savePaymentResponses(List.of(thirdPaymentResponse, fourthPaymentResponse));

        //Then
        assertEquals(2, results.size());
        assertEquals(List.of(id1, id2), results);
        PaymentResponse thirdPaymentReqReturned = paymentRequestDAO.getPaymentResponseById(id1);
        PaymentResponse fourthPaymentReqReturned = paymentRequestDAO.getPaymentResponseById(id2);

        assertEquals(thirdPaymentResponse, thirdPaymentReqReturned);
        assertEquals(fourthPaymentResponse, fourthPaymentReqReturned);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSavePaymentResponsesWithOneEntryHavingNullId() {
        //Given
        Double newAmount = 200.0;
        String id1 = null;
        String id2 = UUID.randomUUID().toString();
        PaymentResponse thirdPaymentResponse = paymentResponse1.toBuilder().id(id1).amount(newAmount).build();
        PaymentResponse fourthPaymentResponse = paymentResponse1.toBuilder().id(id2).amount(newAmount).build();

        //When

        List<String> results = paymentRequestDAO.savePaymentResponses(List.of(thirdPaymentResponse, fourthPaymentResponse));

        //Then
        assertEquals(1, results.size());
        assertEquals(List.of(id2), results);
        PaymentResponse fourthPaymentReqReturned = paymentRequestDAO.getPaymentResponseById(id2);
        assertEquals(fourthPaymentResponse, fourthPaymentReqReturned);
    }
}
