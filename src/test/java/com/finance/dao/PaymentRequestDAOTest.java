package com.finance.dao;

import com.finance.Application;
import com.finance.pojo.PaymentRequest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("integration")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Sql(scripts = {"/sql-data/schema.sql", "/sql-data/data.sql"})
public class PaymentRequestDAOTest {

    public static final String ID = "00026e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final Double AMOUNT = 100.00;
    public static final int TERMS = 2;
    public static final Double RATE = 5.00;
    public static final String PAYMENT_ID = "00026e65-1b70-4e2f-b574-8a3c3aadc6c3";
    public static final int PAYMENT_NUMBER = 1;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());
    private static final Instant CREATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    private static final Instant UPDATED_AT = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();
    public static final Instant PAYMENT_DATE = ZonedDateTime.parse("2021-11-01 21:09:31", FORMATTER).toInstant();

    private static PaymentRequest paymentRequest;

    @Autowired
    private PaymentRequestDAO paymentRequestDAO;

    @Autowired
    private DataSource dataSource;

    @BeforeClass
    public static void initData() {
        paymentRequest = PaymentRequest.builder()
                .id(ID)
                .amount(AMOUNT)
                .rate(RATE)
                .terms(TERMS)
                .createdAt(CREATED_AT)
                .updatedAt(UPDATED_AT)
                .build();
    }

    @Before
    public void init() {
    }

    @Test
    public void testGetPaymentRequestById() {
        //Given
        //When
        PaymentRequest paymentReqReturned = paymentRequestDAO.getPaymentRequestById(ID);

        //Then
        assertEquals(paymentRequest, paymentReqReturned);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testGetPaymentRequestByIdNullMid() {
        //When
        paymentRequestDAO.getPaymentRequestById(null);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testGetPaymentRequestByNoExistentId() {
        //When
        paymentRequestDAO.getPaymentRequestById("other");
    }

    @Test
    public void testSavePaymentRequest() {
        //Given
        Double newAmount = 200.0;
        String id = UUID.randomUUID().toString();
        PaymentRequest secondPaymentRequest = paymentRequest.toBuilder().id(id).amount(newAmount).build();

        //When
        int result = paymentRequestDAO.savePayment(secondPaymentRequest);

        //Then
        assertEquals(1, result);
        PaymentRequest secondPaymentReqReturned = paymentRequestDAO.getPaymentRequestById(id);

        assertEquals(secondPaymentRequest, secondPaymentReqReturned);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testSavePaymentRequestWithNullId() {
        //Given
        Double newAmount = 200.0;
        String id = null;
        PaymentRequest secondPaymentRequest = paymentRequest.toBuilder().id(id).amount(newAmount).build();

        //When
        paymentRequestDAO.savePayment(secondPaymentRequest);
    }
}
