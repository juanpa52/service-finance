CREATE TABLE IF NOT EXISTS payment_requests
(
   id       varchar(36) not null    primary key,
   amount   decimal     null,
   terms    int         null,
   rate     decimal     null,
   created_at                  datetime default CURRENT_TIMESTAMP not null,
   updated_at                  datetime default CURRENT_TIMESTAMP not null
);

CREATE TABLE IF NOT EXISTS payment_responses
(
    id                          varchar(36)                        not null        primary key,
    payment_id                  varchar(36)                        null,
    payment_number              int                                null,
    amount                      decimal                            null,
    payment_date                datetime                           null,
    created_at                  datetime default CURRENT_TIMESTAMP not null,
    updated_at                  datetime default CURRENT_TIMESTAMP not null,

    constraint fk_payment_responses_payment_id
        foreign key (payment_id) references payment_requests (id)
);
