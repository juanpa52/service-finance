DELETE FROM payment_responses;
DELETE FROM payment_requests;

INSERT INTO payment_requests (id, amount, terms, rate, created_at, updated_at) VALUES ('00026e65-1b70-4e2f-b574-8a3c3aadc6c3', 100.00, 2, 5.00, '2021-11-1 21:09:31', '2021-11-1 21:09:31');
INSERT INTO payment_responses (id, payment_id, payment_number, amount, payment_date, created_at, updated_at) VALUES ('11326e65-1b70-4e2f-b574-8a3c3aadc6c3', '00026e65-1b70-4e2f-b574-8a3c3aadc6c3', 1, 55.00, '2021-11-8 21:09:31', '2021-11-1 21:09:31', '2021-11-1 21:09:31');
INSERT INTO payment_responses (id, payment_id, payment_number, amount, payment_date, created_at, updated_at) VALUES ('f014ef1d-53bd-43ee-99a1-4d1c3977be8a', '00026e65-1b70-4e2f-b574-8a3c3aadc6c3', 2, 55.00, '2021-11-15 21:09:31', '2021-11-1 21:09:31', '2021-11-1 21:09:31');
