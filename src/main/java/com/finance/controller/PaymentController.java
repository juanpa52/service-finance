package com.finance.controller;

import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;
import com.finance.service.FinanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/payments")
public class PaymentController {

    @Autowired
    private FinanceService financeService;

    @PostMapping(value = "")
    public ResponseEntity<List<PaymentResponse>> create(@Valid @RequestBody PaymentRequest paymentRequest) {
        log.debug("In PaymentController.create (request = {})", paymentRequest);

        return ResponseEntity.status(HttpStatus.CREATED).body(financeService.createPaymentSet(paymentRequest));
    }

}
