package com.finance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finance.exception.UnexpectedException;
import com.finance.pojo.ErrorField;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    @Qualifier("objectMapper")
    private ObjectMapper mapper;

    @ExceptionHandler(value = {Exception.class , UnexpectedException.class})
    protected ResponseEntity<Object> handleInternalErrorResponse(Exception ex, WebRequest request) {
        log.error("Unexpected exception has happened", ex);

        return handleExceptionInternal(ex, "Service unavailable", new HttpHeaders(), HttpStatus.SERVICE_UNAVAILABLE, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("handleMethodArgumentNotValid received", ex); //For valid anotations in mvc, used with @valid
        final List<ErrorField> validationList = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> mapper.convertValue(error, ErrorField.class))
                .collect(Collectors.toList());

        log.error("Validation error list : {}", validationList);

        return handleExceptionInternal(ex, validationList, new HttpHeaders(), status, request);
    }
}
