package com.finance.dao;

import com.finance.pojo.PaymentRequest;

public interface PaymentRequestDAO extends MySqlBaseDAO<PaymentRequest> {

    PaymentRequest getPaymentRequestById(String id);

    int savePayment(PaymentRequest paymentRequest);

}
