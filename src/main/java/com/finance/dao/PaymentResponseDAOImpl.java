package com.finance.dao;

import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.finance.pojo.FinanceConstants.*;

@Slf4j
@Repository
public class PaymentResponseDAOImpl extends MySqlBaseDAOImpl<PaymentResponse> implements PaymentResponseDAO {

    @Autowired
    public PaymentResponseDAOImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        super(namedParameterJdbcTemplate, PaymentResponse.class);
    }

    private static final List<String> FIELD_LIST = Arrays.asList(ID, PAYMENT_ID, PAYMENT_NUMBER, AMOUNT, PAYMENT_DATE, CREATED_AT, UPDATED_AT);
    private static final List<String> CAMEL_FIELD_LIST = FIELD_LIST.stream().map(field -> CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, field)).collect(Collectors.toList());
    private static final String PAYMENT_RESP_FIELDS = String.join(",", FIELD_LIST);
    private static final String PAYMENT_RESP_INSERT_VALUES = CAMEL_FIELD_LIST.stream().map(field -> ":" + field).collect(Collectors.joining(","));

    private static final String SELECT_PAYMENT_RESP_BY_ID = SELECT + PAYMENT_RESP_FIELDS +
            " FROM payment_responses " +
            " WHERE id=:id ";

    private static final String SELECT_PAYMENT_RESP_BY_PAYMENT_ID = SELECT + PAYMENT_RESP_FIELDS +
            " FROM payment_responses " +
            " WHERE payment_id=:paymentId";

    private static final String INSERT_PAYMENT_RESP_STATEMENT = "INSERT INTO payment_responses"
            + String.format("(%s)", PAYMENT_RESP_FIELDS) +
            " VALUES" + String.format("(%s)", PAYMENT_RESP_INSERT_VALUES);

    @Override
    public PaymentResponse getPaymentResponseById(String id) {
        return queryEntityByField(SELECT_PAYMENT_RESP_BY_ID, ID, id);
    }

    public List<PaymentResponse> getPaymentResponsesByPaymentId(String paymentId){
        final SqlParameterSource params = new MapSqlParameterSource(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, PAYMENT_ID), paymentId);
        return queryEntities(SELECT_PAYMENT_RESP_BY_PAYMENT_ID, params);
    }

    @Override
    public List<String> savePaymentResponses(List<PaymentResponse> paymentResponses) {
        if(paymentResponses.isEmpty())
            return Collections.emptyList();

        final Instant currDate = Instant.now();
        final SqlParameterSource[] params = new SqlParameterSource[paymentResponses.size()];

        for (int i=0; i< paymentResponses.size(); i++) {
            final PaymentResponse paymentResponse = paymentResponses.get(i);
            paymentResponse.setCreatedAt(currDate);
            paymentResponse.setUpdatedAt(currDate);

            params[i] = new BeanPropertySqlParameterSource(paymentResponse) {
                @Override
                public Object getValue(String paramName) {
                    final Object value = super.getValue(paramName);
                    if (value instanceof Enum)
                        return value.toString();

                    return value;
                }
            };
        }

        int[] results = performUpdateBatchOperation(INSERT_PAYMENT_RESP_STATEMENT, params);
        return IntStream.range(0, paymentResponses.size())
                .boxed()
                .filter(i -> results[i] != 0)
                .map(paymentResponses::get)
                .map(PaymentResponse::getId)
                .collect(Collectors.toList());
    }

}
