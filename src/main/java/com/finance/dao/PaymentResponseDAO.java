package com.finance.dao;

import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;

import java.util.List;

public interface PaymentResponseDAO extends MySqlBaseDAO<PaymentResponse> {

    PaymentResponse getPaymentResponseById(String id);

    List<PaymentResponse> getPaymentResponsesByPaymentId(String paymentId);

    List<String> savePaymentResponses(List<PaymentResponse> paymentResponses);
}
