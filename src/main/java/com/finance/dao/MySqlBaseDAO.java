package com.finance.dao;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;

public interface MySqlBaseDAO<T> {

    T queryEntityByField(String sqlOperation, String fieldName, Object fieldValue);

    List<T> queryEntities(String sqlOperation, SqlParameterSource params);

    int performUpdateOperation(String statement, SqlParameterSource params);

    int performUpdateOperation(String statement, Object entity);

    int[] performUpdateBatchOperation(String statement, Object[] entities);

    int[] performUpdateBatchOperation(String statement, SqlParameterSource[] params);

}
