package com.finance.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public abstract class MySqlBaseDAOImpl<T> implements MySqlBaseDAO<T> {

    protected final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    protected final Class<T> entityClass;
    protected final JdbcTemplate jdbcTemplate;

    public MySqlBaseDAOImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, Class<T> entityClass) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.entityClass = entityClass;
        this.jdbcTemplate = namedParameterJdbcTemplate.getJdbcTemplate();
    }

    @Override
    public T queryEntityByField(String sqlOperation, String fieldName, Object fieldValue) {
        final SqlParameterSource params = new MapSqlParameterSource(fieldName, fieldValue);
        return queryForObject(sqlOperation, params);
    }

    @Override
    public int performUpdateOperation(String statement, Object entity) {
        final SqlParameterSource params = new BeanPropertySqlParameterSource(entity) {
            @Override
            public Object getValue(String paramName) throws IllegalArgumentException {
                final Object value = super.getValue(paramName);
                if (value instanceof Enum)
                    return value.toString();

                return value;
            }
        };

        return performUpdateOperation(statement, params);
    }

    @Override
    public int[] performUpdateBatchOperation(String statement, Object[] entities) {
        final SqlParameterSource[] params = new SqlParameterSource[entities.length];
        Arrays.stream(entities)
                .map(BeanPropertySqlParameterSource::new)
                .collect(Collectors.toList())
                .toArray(params);
        return performUpdateBatchOperation(statement, params);
    }

    @Override
    public int performUpdateOperation(String statement, SqlParameterSource params) {
        return namedParameterJdbcTemplate.update(statement, params);
    }

    @Override
    public int[] performUpdateBatchOperation(String statement, SqlParameterSource[] params) {
        return namedParameterJdbcTemplate.batchUpdate(statement, params);
    }

    @Override
    public List<T> queryEntities(String sqlOperation, SqlParameterSource params) {
        return namedParameterJdbcTemplate.query(sqlOperation, params, new BeanPropertyRowMapper<>(entityClass));
    }

    private T queryForObject(String sqlOperation, SqlParameterSource params) {
        return namedParameterJdbcTemplate.queryForObject(sqlOperation, params, new BeanPropertyRowMapper<>(entityClass));
    }

}
