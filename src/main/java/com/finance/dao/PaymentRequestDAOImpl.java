package com.finance.dao;

import com.finance.pojo.PaymentRequest;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.finance.pojo.FinanceConstants.*;

@Slf4j
@Repository
public class PaymentRequestDAOImpl extends MySqlBaseDAOImpl<PaymentRequest> implements PaymentRequestDAO {

    @Autowired
    public PaymentRequestDAOImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        super(namedParameterJdbcTemplate, PaymentRequest.class);
    }

    private static final List<String> FIELD_LIST = Arrays.asList(ID, AMOUNT, TERMS, RATE, CREATED_AT, UPDATED_AT);
    private static final List<String> CAMEL_FIELD_LIST = FIELD_LIST.stream().map(field -> CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, field)).collect(Collectors.toList());
    private static final String PAYMENT_FIELDS = String.join(",", FIELD_LIST);
    private static final String PAYMENT_INSERT_VALUES = CAMEL_FIELD_LIST.stream().map(field -> ":" + field).collect(Collectors.joining(","));

    private static final String SELECT_PAYMENT_BY_ID = SELECT + PAYMENT_FIELDS +
            " FROM payment_requests " +
            " WHERE id=:id ";

    private static final String INSERT_PAYMENT_STATEMENT = "INSERT INTO payment_requests"
            + String.format("(%s)", PAYMENT_FIELDS) +
            " VALUES" + String.format("(%s)", PAYMENT_INSERT_VALUES);

    @Override
    public PaymentRequest getPaymentRequestById(String id) {
        return queryEntityByField(SELECT_PAYMENT_BY_ID, ID, id);
    }

    @Override
    public int savePayment(PaymentRequest paymentRequest) {
        final SqlParameterSource params = new BeanPropertySqlParameterSource(paymentRequest) {
            @Override
            public Object getValue(String paramName) {
                final Object value = super.getValue(paramName);
                if (value instanceof Enum)
                    return value.toString();

                return value;
            }
        };

        final Instant currDate = Instant.now();
        paymentRequest.setCreatedAt(currDate);
        paymentRequest.setUpdatedAt(currDate);

        return performUpdateOperation(INSERT_PAYMENT_STATEMENT, params);
    }
}
