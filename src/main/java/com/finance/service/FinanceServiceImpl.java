package com.finance.service;

import com.finance.dao.PaymentRequestDAO;
import com.finance.dao.PaymentResponseDAO;
import com.finance.exception.UnexpectedException;
import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
public class FinanceServiceImpl implements FinanceService {

    @Autowired
    private PaymentRequestDAO paymentRequestDAO;

    @Autowired
    private PaymentResponseDAO paymentResponseDAO;

    @Transactional
    @Override
    public List<PaymentResponse> createPaymentSet(PaymentRequest paymentRequest) {
        log.debug("Attempting to generate payments for {}", paymentRequest);

        final Double simpleInterest = paymentRequest.getAmount() * (paymentRequest.getRate() / 100.0) * paymentRequest.getTerms();
        final Double weeklyAmount = (paymentRequest.getAmount() + simpleInterest) / paymentRequest.getTerms();
        final String paymentId = UUID.randomUUID().toString();

        paymentRequest.setId(paymentId);
        final List<PaymentResponse> paymentResponses = IntStream.range(1, paymentRequest.getTerms() + 1)
                .boxed()
                .map(i -> PaymentResponse.builder()
                        .id(UUID.randomUUID().toString())
                        .amount(weeklyAmount)
                        .paymentDate(Instant.now().plus(i * 7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS))
                        .paymentNumber(i)
                        .paymentId(paymentId)
                        .build())
                .collect(Collectors.toList());

        paymentRequestDAO.savePayment(paymentRequest);

        Optional.ofNullable(paymentResponseDAO.savePaymentResponses(paymentResponses))
                .filter(responses -> responses.size() == paymentResponses.size())
                .orElseThrow(() -> new UnexpectedException("An unexpected error was avoiding to save payments data"));

        return paymentResponses;
    }

}
