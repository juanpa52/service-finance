package com.finance.service;

import com.finance.pojo.PaymentRequest;
import com.finance.pojo.PaymentResponse;

import java.util.List;

public interface FinanceService {

    List<PaymentResponse> createPaymentSet(PaymentRequest paymentRequest);

}
