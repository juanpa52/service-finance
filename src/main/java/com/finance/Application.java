package com.finance;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import javax.annotation.PreDestroy;
import java.time.Instant;

@Slf4j
@SpringBootApplication(exclude = {
        RedisAutoConfiguration.class,
        RedisRepositoriesAutoConfiguration.class
})
@ComponentScan(
        basePackages = {
                "com.finance"
        },
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = {
                }),
                @ComponentScan.Filter(type = FilterType.REGEX, pattern = {
                })
        })
public class Application{

    public static void main( String[] args ){
        SpringApplication springApplication = new SpringApplication(Application.class);
        springApplication.run(args);
        log.info("service-finance initialized!!!!!!!!!!!!!! at {}", Instant.now());
    }

    @PreDestroy
    public void cleanup() {
        log.info("service-finance is being terminated at {}", Instant.now());
    }
}
