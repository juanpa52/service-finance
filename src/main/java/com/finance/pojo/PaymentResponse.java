package com.finance.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class PaymentResponse extends BaseEntity {

    @JsonIgnore
    private String paymentId;
    private Integer paymentNumber;
    private Double amount;
    private Instant paymentDate;

}
