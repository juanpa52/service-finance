package com.finance.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorField {

    private String field;
    private String defaultMessage;
    private Object rejectedValue;

    @JsonProperty(value = "message", access = JsonProperty.Access.READ_ONLY)
    public String getDefaultMessage() {
        return defaultMessage;
    }

    @JsonProperty(value = "default_message", access = JsonProperty.Access.WRITE_ONLY)
    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

}
