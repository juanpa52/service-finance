package com.finance.pojo;

public final class FinanceConstants {

    private FinanceConstants() {
    }

    public static final String ID = "id";
    public static final String AMOUNT = "amount";
    public static final String TERMS = "terms";
    public static final String RATE = "rate";
    public static final String PAYMENT_ID = "payment_id";
    public static final String PAYMENT_NUMBER = "payment_number";
    public static final String PAYMENT_DATE = "payment_date";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String SELECT = "SELECT ";

}
