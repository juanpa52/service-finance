package com.finance.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.DecimalMin;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest extends BaseEntity {

    @DecimalMin("0.00001")
    private Double amount;

    @Range(min = 1, max = 100)
    private Integer terms;

    @DecimalMin("0.1")
    private Double rate;

}
