package com.finance.exception;

public class UnexpectedException extends RuntimeException {

    public UnexpectedException() {
        super();
    }

    public UnexpectedException(String msg) {
        super(msg);
    }

    public UnexpectedException(Throwable ex) {
        super(ex);
    }

    public UnexpectedException(String msg, Throwable ex) {
        super(msg, ex);
    }
}
