#!/bin/bash
#v.1.0

image=dev-test-java
tags=( $@ )
len=${#tags[@]}

SERVICE_VERSION="$(head -c 6 /dev/urandom | base64)"
echo "service version -> $SERVICE_VERSION"

printHelp() {
  echo
  echo "HELP for 'create-docker-image.sh'"
  echo
  echo "Usage: ./create-docker-image.sh [<tag>] [,<tag>]"
  echo
  echo "Create a docker image"
  echo
  echo "Script takes zero, one, or more tags."
  echo "If one or more tags are provided, the script then tags the docker image using those tags."
  echo "If no tags are provided, 'latest' is assumed to be the single tag to be used."
  echo
  echo
}

createDockerIgnoreFile() {
  echo ".git"                       > .dockerignore
  echo "bin"                       >> .dockerignore
  echo "src/main"                  >> .dockerignore
  echo "src/test"                  >> .dockerignore
  echo "target/scala-*/classes"    >> .dockerignore
  echo "target/streams"            >> .dockerignore
}

processOtherTags() {
  for otherTag in "$@" ; do
    echo "Tagging image $image:$firstTag with $otherTag..."
    docker tag $image:$firstTag $image:$otherTag
  done
}

if [ "$1" == "-?" ] || [ "$1" == "-h" ] ||  [ "$1" == "--h" ] || [ "$1" == "help" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ]
then
  printHelp
  exit 0
fi

if [ $len == 0 ]
then
  echo
  echo "No tags provided, assuming at single tag of 'latest'."
  firstTag=latest
else
  firstTag=${tags[0]}
fi

createDockerIgnoreFile

echo "Creating image $image:$firstTag..."
docker build -f Dockerfile --build-arg serv_vers=$SERVICE_VERSION -t $image:$firstTag .

if [ $len -gt 1 ]
then
  otherTags=("${tags[@]:1:$len}")
  processOtherTags "${otherTags[@]}"
fi

docker images $image