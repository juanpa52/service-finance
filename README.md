# Transaction system with Java and H2

## How to run:

> Requirements to run: 
> *  Docker 
> *  Docker Compose
> *  Maven
> *  Java 11 or higher

1. Looks for the docker-compose file on \service-finance\docker-compose.yml and open a terminal in this path.
2. Run the next command:
    * `docker-compose build`
    * `docker-compose up -d`
3. Request to create a payment set
    * `curl --location --request POST 'http://localhost:8081/payments' \
--header 'Content-Type: application/json' \
--data-raw '{
    "amount": 100.0,
    "terms": 2,
    "rate": 5.00
}'`

4. Response 
    * `[
    {
        "payment_number": 1,
        "amount": 55.0,
        "payment_date": "2021-12-27T00:00:00Z"
    },
    {
        "payment_number": 2,
        "amount": 55.0,
        "payment_date": "2022-01-03T00:00:00Z"
    }
]`

5. Health endpoint
    * `curl --location --request GET 'http://localhost:8081/actuator/health'`
    * `{
    "status": "UP",
    "components": {
        "db": {
            "status": "UP",
            "details": {
                "database": "H2",
                "result": 1,
                "validationQuery": "SELECT 1"
            }
        },
        "diskSpace": {
            "status": "UP",
            "details": {
                "total": 499963174912,
                "free": 347426680832,
                "threshold": 10485760
            }
        },
        "ping": {
            "status": "UP"
        }
    }
}`
