#Getting base image
FROM --platform=linux/amd64 alpine:edge
#Author
MAINTAINER juanpa52 <juanpablo.riveramz@gmail.com>
RUN apk add --no-cache openjdk11
RUN apk update && apk add --no-cache curl
COPY ["/target/service-finance-1.0.jar","/usr/src/"]
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar","/usr/src/service-finance-1.0.jar"]
EXPOSE 8080
